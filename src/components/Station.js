import React from 'react';
import PropTypes from 'prop-types';
import ConnectionList from './ConnectionList';
import StationInformation from './StationInformation';

const Station = props => {

  const connections = props.lines
                      .filter(line => props.undergroundLines.find(undergroundLine => undergroundLine.id === line.id && line.id !== props.currentLine))
                      .map(connection => (<li key={ connection.id } className={ connection.id }>{ connection.name }</li>));

  const information = props.disruptedStopPoints
                      .find(stopPoint => stopPoint.stationAtcoCode === props.id);

  return (
    <div className="station">
      <h2 className="station-name">{ props.name }</h2>
      { information ?
        <StationInformation
          type={ information.type }
          description={ information.description }
        />
      : null }
      { connections.length > 0 ? <ConnectionList>{ connections }</ConnectionList> : null }
    </div>
  );
};

Station.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  lines: PropTypes.array,
  currentLine: PropTypes.string.isRequired,
  undergroundLines: PropTypes.array.isRequired,
  disruptedStopPoints: PropTypes.array.isRequired
};

export default Station;
