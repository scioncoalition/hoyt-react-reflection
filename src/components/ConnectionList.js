import React from 'react';

const ConnectionList = props => {
  return (
    <div className="connections">
      <h3>Connections</h3>
      <ul>
        { props.children }
      </ul>
    </div>
  );
};

export default ConnectionList;
