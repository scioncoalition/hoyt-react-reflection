import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

const UndergroundLine = props => {
  const classList = `${ props.id } severity-level${ props.lineStatus }`
  return (
  <li className={ classList }><NavLink to={ `/line/${ props.id }` }>{ props.name }</NavLink></li>
  )
};

UndergroundLine.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  lineStatus: PropTypes.number
}

export default UndergroundLine;
