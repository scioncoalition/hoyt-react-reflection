import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class RouteSelect extends Component {
  static propTypes = {
    routeList: PropTypes.array.isRequired,
    onRouteSelect: PropTypes.func.isRequired
  };

  state = { selectedRoute: 0 };

  changeRoute = () => {
    this.setState({ selectedRoute: parseInt(this.routeMenu.value, 10)});
  };

  setRoute = (e) => {
    if (e) e.preventDefault();
    this.props.onRouteSelect(this.state.selectedRoute);
  };

  render() {
    const routeSelection = this.props.routeList.map((route, index) => (
      <option key={ index } value={ index }>{ route.name.replace('&harr;', '\u2194') }</option>
    ));

    return (
      <form id="route-selection" onSubmit={this.setRoute}>
        <div className="form-group">
          <label className="route-label" htmlFor="route-list">Select a route</label>
          <div className="row">
            <div className="col-6">
              <select
                ref={(ref) => this.routeMenu = ref}
                onChange={this.changeRoute}
                className="form-control"
                id="route-list">
                { routeSelection }
              </select>
            </div>
            <div className="col-6">
               <button type="submit" className="btn btn-dark">View Route</button>
            </div>
          </div>
        </div>
      </form>
    );
  };
};
