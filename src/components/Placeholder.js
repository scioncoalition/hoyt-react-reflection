import React from 'react';

const Placeholder = () => {
  return (
    <div className="col-9 placeholder">
      <p>Select a line to get station information</p>
    </div>
  );
};

export default Placeholder;
