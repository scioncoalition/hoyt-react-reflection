import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import RouteSelect from './RouteSelect';
import Station from './Station';

export default class Routes extends Component {
  static propTypes = {
    undergroundLines: PropTypes.array.isRequired,
    disruptedStopPoints: PropTypes.array.isRequired,
    match: PropTypes.object
  };

  state = {
    route: 0,
    lineStations: [{
      stationId: 'DSTA001',
      name: 'Default Station',
      lines: []
    }],
    orderedRoutes: [{
        name: 'Default Route',
        naptanIds: ['DSTA001']
      }],
  };

  setRoute = (route) => {
    this.setState({ route });
  };

  getData() {
    axios.get(`https://api.tfl.gov.uk/Line/${this.props.match.params.lineid}/StopPoints?app_id=e6eecb8a&app_key=a64c4055153b074ff48313af99faf748`)
      .then(res => {
        this.setState({
          lineStations: res.data
        });
      })
      .catch(err => {
        console.log('Error fetching and passing data', err)
      })

    axios.get(`https://api.tfl.gov.uk/Line/${this.props.match.params.lineid}/Route/Sequence/all?serviceTypes=Regular&app_id=e6eecb8a&app_key=a64c4055153b074ff48313af99faf748`)
      .then(res => {
        this.setState({
          orderedRoutes: res.data.orderedLineRoutes
        });
      })
      .catch(err => {
        console.log('Error fetching and passing data', err)
      })
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.lineid !== prevProps.match.params.lineid) {
      this.getData();
      this.setRoute(0);
    }
  };

  componentDidMount() {
    this.getData();
  };

  render() {
    const stationList = this.state.orderedRoutes[this.state.route].naptanIds.map(id => {
      const station = this.state.lineStations.find(station => station.naptanId === id);
      if ( !station ) {
        return null
      }
      return (
        <Station
          key={station.naptanId}
          id={station.naptanId}
          name={station.commonName}
          lines={station.lines}
          currentLine={this.props.match.params.lineid}
          undergroundLines={this.props.undergroundLines}
          disruptedStopPoints={this.props.disruptedStopPoints}
        />
      );
    });

    return (
    <div className="col-9" id="routes">
      <RouteSelect
        routeList={ this.state.orderedRoutes }
        onRouteSelect= { this.setRoute }
       />
      <div id="station-list-container" className={ this.props.match.params.lineid }>
      { stationList }
      </div>
    </div>);
  };
};
