import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class StationInformation extends Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
  };

  state = { display: false };

  toggleInformation = () => {
    this.setState({ display: !this.state.display });
  };

  render() {
    return (
      <div className="information">
        <h3
          onClick={ this.toggleInformation }
          className={ this.state.display ? "display-info" : "hide-info" }>{ this.props.type }</h3>
        <p className={ this.state.display ? null : "hidden" }>{ this.props.description }</p>
      </div>
    );
  };
};
