import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import axios from 'axios';
import UndergroundLine from './components/UndergroundLine';
import Placeholder from './components/Placeholder';
import Routes from './components/Routes';

class App extends Component {
  constructor() {
    super();
    this.state = {
      undergroundLines: [],
      disruptedStopPoints: []
    };
  };

  componentDidMount() {
    axios.get('https://api.tfl.gov.uk/Line/Mode/tube/Status?detail=false&app_id=e6eecb8a&app_key=a64c4055153b074ff48313af99faf748')
      .then(res => {
        this.setState({
          undergroundLines: res.data
        });
      })
      .catch(err => {
        console.log('Error fetching and passing data', err)
      })

    axios.get('https://api.tfl.gov.uk/StopPoint/Mode/tube/Disruption?includeRouteBlockedStops=true&app_id=e6eecb8a&app_key=a64c4055153b074ff48313af99faf748')
      .then(res => {
        this.setState({
          disruptedStopPoints: res.data
        });
      })
      .catch(err => {
        console.log('Error fetching and passing data', err)
      })
  };

  render() {
    const undergroundLines = this.state.undergroundLines;
    const lineListComponent = undergroundLines.map(line => (
      <UndergroundLine
        id={ line.id }
        key={ line.id }
        name={ line.name }
        lineStatus={ line.lineStatuses.length > 0 ? line.lineStatuses[0].statusSeverity : 10 } />
    ));

    return (
      <div className="App">
        <header>
          <h1>London Underground</h1>
        </header>
        <BrowserRouter>
          <div className="container-fluid">
            <div className="row">
              <div className="col" id="line-list-container">
                <ul>
                  { lineListComponent }
                </ul>
              </div>
              <Switch>
                <Route exact path="/" component={Placeholder} />
                <Route path="/line/:lineid" component={({match}) => <Routes
                  undergroundLines={ this.state.undergroundLines }
                  disruptedStopPoints={ this.state.disruptedStopPoints }
                  match={ match }
                />} />
              </Switch>
            </div>
          </div>
        </BrowserRouter>
        <footer>
          <p>Powered by <a href="https://tfl.gov.uk/info-for/open-data-users/" target="blank">TFL Open Data</a></p>
        </footer>
      </div>
    );
  }
}

export default App;
